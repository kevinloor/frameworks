import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { RegistrarseComponent } from './app/registrarse/registrarse.component';
import { ModuloComponent } from './app/modulo/modulo.component';

@NgModule({
  declarations: [
    AppComponent,
    RegistrarseComponent,
    ModuloComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
